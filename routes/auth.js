const express = require('express');
const router = express.Router();
const Usuario = require('../models/Usuario');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { authenticateToken } = require('../middleware/authorization');


// @desc     Login
// @route   POST /
router.post('/login', async (req, res) => {
    let user = await Usuario.findOne({
        $and: [
            { $or: [{ nombreUsuario: req.body.nombreUsuario }, { email: req.body.email }] },
            { borrado: false }]
    }).select('+password').exec();

    if (user == null) {
        return res.status(400).json({ message: 'Wrong username or password' });
    }
    try {
        if (await bcrypt.compare(req.body.password, user.password)) {
            jwt.sign({
                _id: user._id,
                email: user.email,
                nombreUsuario: user.nombreUsuario,
                nombres: user.nombres,
                tipo: user.tipo
            }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: 60 * 30 }, (err, token) => {
                res.json({ token });
            });
        } else {
            return res.status(400).json({ message: 'Wrong username or password' });
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// @desc    Update my password
// @route   PATCH /password
router.patch('/password', authenticateToken, async (req, res) => {
    // Especifico para cambiar contraseña
    let usuario = await Usuario.findById(req.usuario._id).select('+password');
    try {
        // validaciones
        if (req.body.previousPassword.length > 0 && req.body.newPassword.length > 0) {
            // validar que el usuario corresponda con el usuario de token
            if (await bcrypt.compare(req.body.previousPassword, usuario.password)) {
                const hashedPassword = await bcrypt.hash(req.body.newPassword, 10);
                usuario.password = hashedPassword;
                await usuario.save();
                const updatedUsuario = await Usuario.findById(req.usuario._id);
                res.status(200).json(updatedUsuario);
            } else {
                return res.status(400).json({ message: 'Wrong previous password' });
            }
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// @desc    Delete my account
// @route   DELETE /cancel-my-account
router.delete('/cancel-my-account', [authenticateToken], async (req, res) => {
    let usuario = await Usuario.findById(req.usuario._id).select('+password');
    try {
        // validar que el usuario corresponda con el usuario de token por contraseña
        if (await bcrypt.compare(req.body.password, usuario.password)) {
            usuario['borrado'] = true;
            await usuario.save();
            res.json({ message: 'Deleted Usuario' });
        } else {
            return res.status(400).json({ message: 'Wrong password' });
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
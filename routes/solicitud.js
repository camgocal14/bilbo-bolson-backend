const express = require('express');
const router = express.Router();
const Solicitud = require('../models/Solicitud');
const Respuesta = require('../models/Respuesta');
const { authenticateToken, authorizeUserType } = require('../middleware/authorization');
const { enumAreaEncargada, enumEstadoSolicitud, enumTipoSolicitud, enumTipoUsuario } = require('../config/enums');


// @desc    Gets all
// @route   GET /
router.get('/', [authenticateToken, authorizeUserType([enumTipoUsuario.ASESOR, enumTipoUsuario.ADMIN])], async (req, res) => {
    try {

        const query = req.query;
        let solicitudes;
        if (query.q != null) {
            solicitudes = await Solicitud.find({
                "$or": [
                    { "titulo": { $regex: query.q, $options: "i" } },
                    { "descripcion": { $regex: query.q, $options: "i" } },
                    // { "estado": { $regex: query.q, $options: "i" } },
                    // { "areaEncargada": { $regex: query.q, $options: "i" } },
                ]
            }).where('borrado').equals(false).populate({ path: 'respuestas', match: { 'borrado': false } });
        } else {
            solicitudes = await Solicitud.find(query).where('borrado').equals(false).populate({ path: 'respuestas', match: { 'borrado': false } });
        }

        res.json(solicitudes);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// @desc    Mi solicitud desde cliente
// @route   GET /
router.get('/cli/:id', authenticateToken, async (req, res) => {
    let solicitud;
    try {
        let id = req.params.id;
        const clientId = req.usuario._id;
        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            return res.status(400).json({ message: 'Solicitud ID has wrong format.' });
        }
        solicitud = await Solicitud.findById(id).populate({ path: 'respuestas', match: { 'borrado': false } });
        if (solicitud == null) {
            return res.status(404).json({ message: 'Cannot find solicitud' });
        }
        if (solicitud.solicitante != clientId) {
            return res.sendStatus(403);
        }
        res.json(solicitud);
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
});

// @desc    Mis solicitudes
// @route   GET /
router.get('/mis-solicitudes', authenticateToken, async (req, res) => {
    try {
        const idUsuario = req.usuario._id;
        const solicitudes = await Solicitud.find({
            "$or": [
                { "solicitante": idUsuario },
                { "asesor": idUsuario }
            ]
        }).where('borrado').equals(false).populate({ path: 'respuestas', match: { 'borrado': false } });

        res.json(solicitudes);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// @desc    Get one
// @route   GET /:id
router.get('/:id', [authenticateToken, authorizeUserType([enumTipoUsuario.ASESOR, enumTipoUsuario.ADMIN]), getSolicitud], async (req, res) => {
    await res.json(res.solicitud);
});

// @desc    Create one
// @route   POST /
router.post('/', [
    authenticateToken,
    authorizeUserType([enumTipoUsuario.CLIENTE])], async (req, res) => {
        try {
            const solicitud = new Solicitud({
                ...req.body,
                solicitante: req.usuario?._id,
                estado: enumEstadoSolicitud.SIN_ATENDER,
            });
            const newSolicitud = await solicitud.save();
            res.status(201).json(newSolicitud);
        } catch (err) {
            res.status(400).json({ message: err.message });
        }
    });

// @desc    Update one
// @route   PATCH /:id
router.patch('/:id', [authenticateToken, getSolicitud], async (req, res) => {
    // Valida que solo el solicitante pueda modificar su propia solicitud
    if (res.solicitud.solicitante != req.usuario._id) {
        return res.sendStatus(403);
    }

    const objKeys = Object.keys(res.solicitud._doc).filter(v => (v !== '_id') && (v !== '__v'));
    objKeys.forEach(key => {
        if (req.body[key] != null) res.solicitud[key] = req.body[key];
    });
    res.solicitud['fechaActualizacion'] = Date.now();
    try {
        const updatedSolicitud = await res.solicitud.save();
        res.json(updatedSolicitud);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// @desc    Remision
// @route   PATCH /remitir/:id
router.patch('/remitir/:id', [authenticateToken, authorizeUserType([enumTipoUsuario.ASESOR, enumTipoUsuario.ADMIN]), getSolicitud], async (req, res) => {
    // const objKeys = Object.keys(res.solicitud._doc).filter(v => (v !== '_id') && (v !== '__v'));

    if (req.body['areaEncargada'] != null) res.solicitud['areaEncargada'] = req.body['areaEncargada'];

    res.solicitud['estado'] = enumEstadoSolicitud.REDIRIGIDA;
    res.solicitud['fechaActualizacion'] = Date.now();
    try {
        const updatedSolicitud = await res.solicitud.save();
        res.json(updatedSolicitud);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// @desc    Add respuesta
// @route   POST /:id/respuesta
router.post('/:id/respuesta', [authenticateToken, getSolicitud], async (req, res) => {
    console.log(req.usuario);
    if (res.solicitud.solicitante != req.usuario._id && req.usuario.tipo.toUpperCase() != enumTipoUsuario.ASESOR && req.usuario.tipo.toUpperCase() != enumTipoUsuario.ADMIN) {
        return res.status(400).json({ message: "Para responder debes ser el autor de la solicitud, un asesor o un administrador" });
    }

    try {
        const respuesta = new Respuesta({
            ...req.body,
            usuario: req.usuario._id
        });
        const newRespuesta = await respuesta.save();

        res.solicitud.respuestas.push(newRespuesta._id);
        if (req.usuario.tipo.toUpperCase() == enumTipoUsuario.ASESOR) {
            //actualizar el ultimo asesor que atendio esta soliciud
            res.solicitud.asesor = req.usuario._id;
            res.solicitud.estado = enumEstadoSolicitud.ATENDIDA;
        }
        try {
            const updatedSolicitud = await res.solicitud.save();
            Solicitud.findById(updatedSolicitud._id).populate({ path: 'respuestas', match: { 'borrado': false } }).exec((err, sol) => {
                res.json(sol);
            });
            // res.json(updatedSolicitud);
        } catch (error) {
            res.status(400).json({ message: error.message });
        }
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});


/// Solo los administradores pueden borrar solicitudes y respuestas
/// los usuarios pueden editar sus propias solicitudes y respuestas o darlas por terminadas

// @desc    Delete one
// @route   DELETE /:id
router.delete('/:id', [authenticateToken, authorizeUserType([enumTipoUsuario.ADMIN]), getSolicitud], async (req, res) => {
    try {
        res.solicitud['borrado'] = true;
        await res.solicitud.save();
        res.json({ message: 'Deleted Solicitud' });
    } catch (error) {
        res.status(500).json({ message: err.message });
    }
});

// @desc    Delete one
// @route   DELETE /:id
router.delete('/respuesta/:id', [authenticateToken, authorizeUserType([enumTipoUsuario.ADMIN]), getSolicitud], async (req, res) => {
    try {
        res.respuesta['borrado'] = true;
        await res.respuesta.save();
        res.json({ message: 'Deleted Respuesta' });
    } catch (error) {
        res.status(500).json({ message: err.message });
    }
});

async function getSolicitud(req, res, next) {
    let solicitud;
    try {
        let id = req.params.id;
        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            return res.status(400).json({ message: 'Solicitud ID has wrong format.' });
        }
        solicitud = await Solicitud.findById(id);
        if (solicitud == null) {
            return res.status(404).json({ message: 'Cannot find solicitud' });
        }
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    res.solicitud = solicitud;
    next();
}

async function getRespuesta(req, res, next) {
    let respuesta;
    try {
        let id = req.params.id;
        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            return res.status(400).json({ message: 'Respuesta ID has wrong format.' });
        }
        respuesta = await Respuesta.findById(id);
        if (respuesta == null) {
            return res.status(404).json({ message: 'Cannot find respuesta' });
        }
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    res.respuesta = respuesta;
    next();
}

module.exports = router;
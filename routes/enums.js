const express = require('express');
const router = express.Router();
const { enumAreaEncargada, enumEstadoSolicitud, enumTipoSolicitud, enumTipoUsuario } = require('../config/enums');

// @desc    Gets all
// @route   GET /
router.get('/', (req, res) => {
    try {
        // console.log(enumAreaEncargada);
        res.json({
            data: {
                enumAreaEncargada,
                enumEstadoSolicitud,
                enumTipoSolicitud,
                enumTipoUsuario
            }
        });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
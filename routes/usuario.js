const express = require('express');
const router = express.Router();
const Usuario = require('../models/Usuario');
const bcrypt = require('bcrypt');
const { authenticateToken, authorizeUserType } = require('../middleware/authorization');
const { enumTipoUsuario } = require('../config/enums');

// @desc    Gets all
// @route   GET /
router.get('/', [authenticateToken, authorizeUserType([enumTipoUsuario.ASESOR, enumTipoUsuario.ADMIN])], async (req, res) => {
    try {
        const usuarios = await Usuario.find().where('borrado').equals(false);
        res.json(usuarios);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


// @desc    Get one
// @route   GET /mi-perfil
router.get('/mi-perfil', authenticateToken, async (req, res) => {
    try {
        const usuario = await Usuario.findById(req.usuario._id);
        return res.json(usuario);
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
});

// @desc    Get one
// @route   GET /:id
router.get('/:id', [authenticateToken, authorizeUserType([enumTipoUsuario.ASESOR, enumTipoUsuario.ADMIN]), getUsuario], (req, res) => {
    res.json(res.usuario);
});

// @desc    Create one
// @route   POST /
router.post('/', async (req, res) => {
    try {
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        const usuario = new Usuario({
            ...req.body,
            password: hashedPassword
        });
        const newUsuario = await usuario.save();
        res.status(201).json(newUsuario);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// @desc    Update my user
// @route   PATCH /
router.patch('/', authenticateToken, async (req, res) => {
    // Solo se autoriza a modificar el usuario con el que se está logueado, no se deben modificar otros usuarios
    let usuario = await Usuario.findById(req.usuario._id);
    let objKeys = Object.keys(usuario._doc).filter(v => (v !== '_id') && (v !== '__v'));
    objKeys.forEach(key => {
        if (req.body[key] != null) usuario[key] = req.body[key];
    });
    try {
        const updatedUsuario = await usuario.save();
        res.json(updatedUsuario);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});


// @desc    Delete one
// @route   DELETE /:id
router.delete('/:id', [authenticateToken, authorizeUserType([enumTipoUsuario.ADMIN]), getUsuario], async (req, res) => {
    // Solo admin puede borrar otros usuario
    try {
        // await res.usuario.remove();
        res.usuario['borrado'] = true;
        await res.usuario.save();
        res.json({ message: 'Deleted Usuario' });
    } catch (error) {
        res.status(500).json({ message: err.message });
    }
});



async function getUsuario(req, res, next) {
    let usuario;
    try {
        let id = req.params.id;
        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            return res.status(400).json({ message: 'Usuario ID has wrong format.' });
        }
        usuario = await Usuario.findById(id);
        if (usuario == null) {
            return res.status(404).json({ message: 'Cannot find usuario' });
        }
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    res.usuario = usuario;
    next();
}

module.exports = router;
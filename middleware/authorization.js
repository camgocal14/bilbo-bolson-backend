const jwt = require('jsonwebtoken');

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, usuario) => {
        if (err) {
            console.error(err);
            return res.sendStatus(401);
        };
        req.usuario = usuario;
        next();
    });
};

const authorizeUserType = (permissions) => {
    return (req, res, next) => {
        const tipoUsuario = req.usuario?.tipo.toUpperCase();
        if (permissions.includes(tipoUsuario)) {
            next();
        } else {
            return res.sendStatus(401);
        }
    };
};


module.exports = { authenticateToken, authorizeUserType };
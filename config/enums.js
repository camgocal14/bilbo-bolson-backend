const enumTipoUsuario = {
    ADMIN: 'ADMIN',
    CLIENTE: 'CLIENTE',
    ASESOR: 'ASESOR',
};

const enumTipoSolicitud = {
    PETICION: 'PETICION',
    RECLAMO: 'RECLAMO',
    FELICITACION: 'FELICITACION',
};

const enumAreaEncargada = {
    GERENCIA: 'GERENCIA',
    ADMINISTRACION: 'ADMINISTRACION',
    RECURSOS_HUMANOS: 'RECURSOS HUMANOS',
    SOPORTE: 'SOPORTE',
    CONTABILIDAD: 'CONTABILIDAD',
    PRODUCCION: 'PRODUCCION',
    LEGAL: 'LEGAL',
};

const enumEstadoSolicitud = {
    SIN_ATENDER: 'SIN ATENDER',
    RECIBIDA: 'RECIBIDA',
    ATENDIDA: 'ATENDIDA',
    REDIRIGIDA: 'REDIRIGIDA',
    CERRADA: 'CERRADA',
};
module.exports = { enumAreaEncargada, enumEstadoSolicitud, enumTipoSolicitud, enumTipoUsuario };
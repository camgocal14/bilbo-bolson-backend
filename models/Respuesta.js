const mongoose = require('mongoose');

const RespuestaSchema = new mongoose.Schema({
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true,
    },
    contenido: {
        type: String,
        required: true,
    },
    fecha: {
        type: Date,
        immutable: true,
        default: () => Date.now()
    },
    borrado: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Respuesta', RespuestaSchema);

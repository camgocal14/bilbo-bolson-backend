const mongoose = require('mongoose');

const validateEmail = (email) => {
    return email.match(
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

const UsuarioSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        unique: true,
        validate: [validateEmail, "Please fill a valid email address"],
    },
    nombreUsuario: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    nombres: { //nombres y apellidos
        type: String,
        required: true
    },
    tipo: { // cliente o asesor
        type: String,
        required: true
    },
    identificacion: {
        type: String,
        required: true
    },
    direccion: {
        type: String,
    },
    barrio: {
        type: String,
    },
    ciudad: {
        type: String,
    },
    numeroTelefonico: {
        type: String,
    },
    fechaDeCreacion: {
        type: Date,
        immutable: true,
        default: () => Date.now()
    },
    datosAsesor: {
        areaEncargada: {
            type: String
        },
        rating: {
            type: Number
        },
    },
    borrado: {
        type: Boolean,
        default: false
    }
});



module.exports = mongoose.model('Usuario', UsuarioSchema);
const mongoose = require('mongoose');

const SolicitudSchema = new mongoose.Schema({
    titulo: {
        type: String,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    prioridad: {
        type: Number,
        min: 1,
        max: 5,
    },
    estado: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    fechaSolicitud: {
        type: Date,
        immutable: true,
        default: () => Date.now()
    },
    fechaActualizacion: {
        type: Date,
        default: () => Date.now()
    },
    rating: {
        type: Number,
        min: 1,
        max: 5,
    },
    areaEncargada: {
        type: String,
        required: true,
    },
    solicitante: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true,
    },
    asesor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario',
    },
    respuestas: {
        type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Respuesta' }],
        required: false,
    },
    borrado: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Solicitud', SolicitudSchema);

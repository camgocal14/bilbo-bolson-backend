const express = require('express');
const connectDB = require('./config/db');
const cors = require('cors');
require('dotenv').config();

connectDB();
const app = express();

// Accesp JSON in requests
app.use(express.json());

// Allow cors all origins
app.use(cors());

// Routes
app.use('/', require('./routes/index'));
app.use('/api/usuario', require('./routes/usuario'));
app.use('/api/solicitud', require('./routes/solicitud'));
app.use('/api/enums', require('./routes/enums'));
app.use('/api/auth', require('./routes/auth'));


const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`));